import zmq
import logging
import time
import json
from collections import deque



class SonzoServ:
    """
    Sonzo Client Door Server Class
    """
    
    def __init__(self, config):
        """
        Initialize the SonzoServ Server.
        """
        if not validateDoorConfig(config):
            logging.error("Configuration is not valid or incomplete.")
            return False
            
        self._in = deque()
        self._out = deque()
        self._state = False
        self._name = config['NAME']
        self._ip = config['IP']
        self._port = config['PORT']
        self._id = config['ID']
        
        # Prebuilt messages
        self.HEARTBEAT = {'TYPE': 'HEARTBEAT', 'USER': "", 'DOOR': '{}'.format(self._name), 'MESSAGE': ""}
        self.HEARTBEAT_RESPONSE = {'TYPE': 'HEARTBEAT_RESPONSE', 'USER': "", 'DOOR': '{}'.format(self._name), 'MESSAGE': ""}
        self.CONNECT = {'TYPE': 'CONNECT', 'USER': "", 'DOOR': '{}'.format(self._name), 'MESSAGE': ''}
        
        # Heartbeat tracking vars
        self._last_message = time.mktime(time.gmtime())
        self._heartbeat_timeout = 5
        self._missed_heartbeats = 0
        self._max_missed_heartbeats = 3
        self._last_heartbeat_sent = 0

        
        try:
            context = zmq.Context()
            self.sock = context.socket(zmq.PAIR)
            self.sock.connect("tcp://localhost:%s" % self._port)
            self._state = True
        except:
           self._state = False

        return

     
    def recievedMessages(self, messages):
        """
        This function is called by when new messages are recieved.
 
        This function should be overridden by the library user tobytes
        handle new incoming messages.
        """
        pass
        
        
    def run(self):
        """
        Run SonzoServ Server
        """
        while True:
            self.processDoorQueues()
        #    self._sendMessages()
        #    msg = self._recieveMessages()
            time.sleep(1)                
      
    
    def processDoorQueues(self):
        """
        Process outgoing door messages
        and return incoming door messages.
        """
        self._sendMessages()
        self._recieveMessages()
        if self._in:
            messages = self._in
            self._in.clear()
            return messages
        
        self._checkHeartbeat()
                    
        return False  


    def _checkHeartbeat(self):
        """
        Check heartbeat.
        """
        now = time.mktime(time.gmtime())
            
        if self._last_message + self._heartbeat_timeout < now:
            self._missed_heartbeats = self._missed_heartbeats + 1
            if self._last_heartbeat_sent + 5 < now: 
                self._last_heartbeat_sent = now
                self.sendMessage(self.HEARTBEAT)
            
        
    def sendMessage(self, message):
        """
        Add new message to the outgoing queue.
        """
        self._out.append(message)

        
    def _recieveMessages(self):
        """
        Receive and queue all messages.
        """
        messages = deque()
        while True:
            try:
                msg = self.sock.recv(zmq.DONTWAIT)
            except:
                if messages:
                    self.recievedMessages(messages)
                return
                
            m = self._unpackDoorMessage(msg)
            self._last_message = time.mktime(time.gmtime())
            self._missed_heartbeats = 0
            if m['TYPE'] == 'HEARTBEAT':
                self.sendMessage(self.HEARTBEAT_RESPONSE)
            elif m['TYPE'] == 'HEARTBEAT_RESPONSE':
                pass
            else:
                 messages.append(m)
                            
        return
            
                
    def _sendMessages(self):
        """
        Send all waiting messages to door.
        """
        while self._out:
            msg = self._packDoorMessage(self._out.pop())
            try:
                self.sock.send(msg.encode('ascii','ignore'))
            except:
                logging.error("Error: Failed sending zmq message: {}".format(msg))
                
            
    def _unpackDoorMessage(self, message):
        """
        Convert door message back to a dictionary.
        """
        msg = {}
    
        try:
            msg = json.loads(message.decode('ascii','ignore'))
        except:
            logging.error("Error: Unable to unserialize message: {}".format(msg,))
            return False
        
        # Ensure the messages has all required fields
        if self._validateDoorMessage(msg, False):
            return msg  
        else:
            return False
    
    
    def _packDoorMessage(self, message):
        """
        Create a door message.
        """
   
        # Ensure the messages has all required fields
        if self._validateDoorMessage(message, True):
            try:      
                msg = json.dumps(message)
            except:
                logging.error("Error: Unable to serializing message: {}".format(message))
                return False
            
            return msg
        else:
            return False
            
            
    def _validateDoorMessage(self, message, packing):  
        """
        Validate door message contains the correct fields.
        """
        if 'TYPE' in message.keys() and \
                  'USER' in message.keys() and \
                  'DOOR' in message.keys() and \
                  'MESSAGE' in message.keys():
            return True
            
        else:
            if packing:
                logging.error("Error: Cannot pack invalid Door Message: {}".format(message))
            else:
                logging.error("Error: Cannot unpack invalid Door Message: {}".format(message))
                
            return False        
        
        
        
        
        
        
        
        
        
        
        
        
def validateDoorConfig(config):  
    """
    Validate door message contains the correct fields.
    """
    if 'NAME' in config.keys() and \
              'IP' in config.keys() and \
              'PORT' in config.keys() and \
              'ID' in config.keys():
        return True
               
    return False
    
    
    
    
if __name__ == '__main__':
    cfgs = []
    first = {}
    first['PORT'] = '2222'
    first['NAME'] = 'Test Door'
    first['IP'] = '*'
    first['ID'] = 'TELECONFERENCE'
    
    cfgs.append(first)
    x = SonzoServ(first)
    x.run()
    #print(x)