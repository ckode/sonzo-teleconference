from sonzolib.sonzoserv import SonzoServ, validateDoorConfig

import logging
import time
import sys
import re


DISCONNECT_MESSAGE = {'TYPE': 'DISCONNECT', 'USER': "", 'DOOR': 'TELECONFERENCE', 'MESSAGE': ""}



class User:
    """
    Teleconference User Class
    """
    def __init__(self, username):
        """
        Initialize Teleconference User Class
        """
        
        self.name = username
        self.channel = "PUBLIC"
        self.whispers = True
        self.shouts = True



        
class Channel:
    """
    Teleconference Channel Class.
    """
    
    def __init__(self, owner, name):
        """
        Initialize Teleconference Channel Class.
        """
        self.owner = owner
        self.name = name
        self.password = None
        self.users = {}
        
    def getUsersInChannel(self):
        """
        Return all users in channel.
        """
        return self.users.keys()
        
        
    def removeFromChannel(self, user):
        """
        Remove user from channel.
        """
        if user in self.users.keys():
            del self.users[user]
        # If there are users still in room, or room is the public room, don't delete it. (True doesn't, False will delete it)
        if self.users or self.name == 'PUBLIC':
            return True
        else:
            return False
            
            
    def _kickUser(self, kicker, kicked):
        """
        Kick a user from the channel. (must be channel owner or moderator)
        """
        if kicker == self.owner:
            # Todo: Create kick channel code
            pass
            
     
        
        
class Teleconference(SonzoServ):
    """
    Teleconference door for SonzoBBS
    """
   
    def __init__(self, config):
        """
        Initialize Teleconference Door for Sonzo BBS
        """
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG) 
        
        if not validateDoorConfig(config):
            logging.error("Error: Invalid teleconference door configuration.  Missing required field(s).\n")
            sys.exit(1)

        SonzoServ.__init__(self, config) 
        self.door = config['DOOR']
        self.name = config['NAME']
        self.users = {}
        self.channels = {}
        self.channels['PUBLIC'] = Channel('SYSOP', 'PUBLIC')
        
        

    def recievedMessages(self, messages):
        """
        Send all new messages to the message router.
        """
        for msg in messages:
            self.routeMessage(msg)
            
            
    def routeMessage(self, message):
        """
        Route incoming message to correct message handler.
        """
        if message['TYPE'] == 'SYSTEM':
            self.systemMessage(message)
            return
        elif message['TYPE'] == 'USER':
            self.messageParser(message)
            return
        elif message['TYPE'] == 'GLOBAL':
            self.sendGlobalMessage(message)
        elif message['TYPE'] == 'DISCONNECT':
            self.userHungup(message)
            return
        elif message['TYPE'] == 'CONNECT':
            self.onConnect(message)
            return
            
            
        
    def messageParser(self, msg):
        """
        Parse and expidite user messages.
        """
        
        commands = {'/quit': "",
                    '/leave': "",
                    '/whisper': "",
                    '/join': ""
                   }
                   
        line = msg['MESSAGE']
        cmd = line.split()
        cmdstr = re.compile(re.escape(cmd[0].lower()))

        for each in commands.keys():
            if cmdstr.match(each):         
                if each == "/quit" and len(cmd[0]) == 1:
                    if self.onQuit(msg['USER']):
                        return
                elif each == "/leave" and len(cmd[0]) == 1:
                    if self.onLeaveChannel(msg['USER']):
                        return
        

        
    def forwardGlobalMessage(self, message):
        """
        Forward global messages to the user.
        """
        msg = message['MESSAGE']
        user = message['USER']
        self.sendMessage(self.makeUserMessage(user, msg))
        
                
    def systemMessage(self, msg):
        """
        Handle System Messages.
        """
        pass

        
    def onConnect(self, msg):
        """
        Handle new connections to Teleconference.
        """
        logging.info(" User connected to the door.")
        user = msg['USER']
        self.users[user] = User(user)
        self.sendMessage(self.makeUserMessage(user, "Welcome to Teleconference!"))
        
        #pass
        
        
    def onDisconnect(self, msg):
        """
        Clean up after a user disconnects.
        """
        pass
        
        
    def userHungup(self, msg):
        """
        Handle users that unexpectedly hung up.
        """
        if user in self.users.keys():
            self.sendToChannel(u, "{} just hung up!!!.")
            self.userQuit(user)   

            
    def onLeaveChannel(self, user):  
        """
        Handle user leaving a channel.
        """
        if user in self.users.keys():
            u = self.users['USER'] 
            self.leaveChannel(user)
            self.sendToChannel(u, "{} has left the channel.")
            if u.channel == 'PUBLIC':
                self.userQuit(user)
        
        
    def leaveChannel(self, user):
        """
        Disconnect user from channel
        """
        self.channels.removeFromChannel(user)
        
        
        
    def onQuit(self, user):
        """
        Handle user leaving teleconference.
        """
        if user in self.users.keys():
            self.sendToChannel(u, "{} has left teleconference.")
            self.userQuit(user)
 
            
    def userQuit(self, user):
        """
        Log player out of Teleconference.
        """
        global DISCONNECT_MESSAGE
        BYE = DISCONNECT_MESSAGE
        if user in self.users.keys():
            u = self.users['USER']
            BYE['USER'] = user
            del self.users['user']
            self.teleconference.sendMessage(BYE)
            
    def sendToChannel(self, user, message):
        """
        Send message to channel.
        """
        if user.channel in self.channels.keys():
            msg = "^G{} say, ^w{}".format(user.name, message)
            for user in self.channels.getUsersInChannel():
                self.sendMessage(self, self.makeUserMessage(user, msg))

        #else:
                    
    def makeUserMessage(self, user, message):
        """
        Build a normal message (dictionary).
        """        
        msg = {'TYPE': 'USER',
               'USER': user,
               'DOOR': self.door,
               'MESSAGE': message
              }
        return msg      
        
if __name__ == '__main__':
    cfgs = []
    first = {}
    first['PORT'] = '2222'
    first['NAME'] = 'Teleconference'
    first['IP'] = 'localhost'
    first['ID'] = 'TELECONFERENCE'
    first['DOOR'] = 'TELECONFERENCE'
    
    cfgs.append(first)
    x = Teleconference(first)
    CONNECT = {'USER': '', 'DOOR': 'NEWDOOR', 'MESSAGE': 'WOZER!', 'TYPE': 'CONNECT' }
    x.sendMessage(CONNECT)
    x.run()
  